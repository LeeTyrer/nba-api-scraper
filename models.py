from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, String, Date
from sqlalchemy.sql.sqltypes import Boolean, Float, Integer

Base = declarative_base()

# Seasons
class Seasons(Base):
    __tablename__ = "seasons"
    id = Column(String, primary_key=True)
    year = Column(String, nullable=False)
    type_code = Column(String)
    type_name = Column(String)
    start_date = Column(Date)
    end_date = Column(Date)
    status = Column(String)

class Players(Base):
    __tablename__ = "players"
    player_id = Column(Integer, primary_key=True)
    team_id = Column(Integer, nullable=False)
    full_name = Column(String)
    roster_status = Column(Boolean)
    start_year = Column(Integer)
    end_year = Column(Integer)

class Teams(Base):
    __tablename__ = "teams"
    team_id = Column(Integer, primary_key=True)
    abbreviation = Column(String)
    team = Column(String)
    year_founded = Column(Integer)
    city = Column(String)

class PlayerGameLogs(Base):
    __tablename__ = "player_gamelogs"
    player_id = Column(Integer, primary_key=True)
    team_id = Column(Integer, primary_key=True)
    game_id = Column(Integer, primary_key=True)
    season = Column(String)
    date = Column(Date)
    matchup = Column(String)
    outcome = Column(String)
    minutes_played = Column(Float)
    field_goals_made = Column(Integer)
    field_goals_attempted = Column(Integer)
    field_goals_percentage = Column(Float)
    three_pointers_made = Column(Integer)
    three_pointers_attempted = Column(Integer)
    three_points_percentage = Column(Float)
    free_throws_made = Column(Integer)
    free_throws_attempted = Column(Integer)
    free_throws_percentage = Column(Float)
    offensive_rebounds = Column(Integer)
    defensive_rebounds = Column(Integer)
    rebounds = Column(Integer)
    turnovers = Column(Integer)
    steals = Column(Integer)
    blocked_shots = Column(Integer)
    blocked_assists = Column(Integer)
    personal_fouls = Column(Integer)
    points = Column(Integer)

class AdvancedPlayerGameLogs(Base):
    __tablename__ = "player_advanced_gamelogs"
    player_id = Column(Integer, primary_key=True)
    team_id = Column(Integer, primary_key=True)
    game_id = Column(String, primary_key=True)
    season = Column(String)
    date = Column(Date)
    matchup = Column(String)
    outcome = Column(String)
    eff_offensive_rating = Column(Float)
    offensive_rating = Column(Float)
    eff_defensive_rating = Column(Float)
    defensive_rating = Column(Float)
    eff_net_rating = Column(Float)
    net_rating = Column(Float)
    assist_percentage = Column(Float)
    assist_to_turnover = Column(Float)
    assist_ratio = Column(Float)
    offensive_rebound_percentage = Column(Float)
    defensive_rebound_percentage = Column(Float)
    rebound_percentage = Column(Float)
    team_turnover_percentage = Column(Float)
    eff_turnover_percentage = Column(Float)
    eff_field_goal_percentage = Column(Float)
    true_shooting_percentage = Column(Float)
    usage_percentage = Column(Float)
    eff_usage_percentage = Column(Float)
    eff_pace = Column(Float)
    pace = Column(Float)
    pace_per_40 = Column(Float)
    possessions = Column(Integer)
    player_impact_estimate = Column(Float)
