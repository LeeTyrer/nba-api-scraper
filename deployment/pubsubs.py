import os
from google.cloud import pubsub_v1
from google.cloud import scheduler_v1

PROJECT_NAME = "nba-api-scraper"
NAMES = ["hourly-api-calls", "daily-api-calls", "weekly-api-calls"]
LOCATION = "australia-southeast1"

publisher = pubsub_v1.PublisherClient()
scheduler = scheduler_v1.CloudSchedulerClient()

def deploy_pubsub_topic(project: str, name: str):
    project = PROJECT_NAME
    topics = publisher.list_topics(project=f"projects/{project}").topics
    topic_path = publisher.topic_path(project, name)
    for topic in topics:
        if topic.name == topic_path:
            return
    publisher.create_topic(name=topic_path)

def deploy_scheduler(project: str, location: str, name: str, topic: str):
    locations_path = scheduler.common_location_path(project, location)
    topics_path = scheduler.topic_path(project, topic)
    jobs_path = scheduler.job_path(project, location, name)
    page_generator = scheduler.list_jobs(parent=locations_path).pages
    jobs = [page.name for pages in page_generator for page in pages.jobs]
    job_file = scheduler_v1.Job(
        name=jobs_path,
        description="Scheduler for API calls", 
        pubsub_target={"topic_name": topics_path, "data": str.encode("Timer has run")},
        schedule="0 */3 * * *",
        time_zone="Australia/Brisbane"
    )
    for job in jobs:
        if job == jobs_path:
            scheduler.update_job(job=job_file)
            return
    scheduler.create_job(parent=locations_path, job=job_file)

for topic in NAMES:
    deploy_pubsub_topic(PROJECT_NAME, topic)
    deploy_scheduler(PROJECT_NAME, LOCATION, topic, topic)