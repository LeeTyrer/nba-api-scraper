from typing import List

class DataSetNotFoundError(Exception):
    """Exception raised for datasets not found in available list"""

    def __init__(self, dataset, datasets, message="could not be found"):
        """

        Args:
            dataset: key being searched in the datasets
            datasets: available datasets
            message: `could not be found`
        """

        self.dataset = dataset
        self.datasets = datasets
        self.message = message
        super().__init__(self.message)
    
    def __str__(self) -> str:
        return f"'{self.dataset}' {self.message}. Please choose from {self.available_datasets}"

    @property
    def available_datasets(self) -> List:
        return [dataset for dataset in self.datasets]
