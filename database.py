import pandas as pd
import sqlalchemy
import psycopg2
import os
import numpy as np

from typing import Any, Tuple, Union
from dataclasses import dataclass
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.inspection import inspect

from models import Base


# Global adapter
def nan_to_null(f,
                _NULL=psycopg2.extensions.AsIs("NULL"),
                _Float=psycopg2.extensions.Float):
    """Numpy NaN is a float, requiring an adapter to convert into NULL
        Refer to: 
        https://stackoverflow.com/questions/32107558/how-do-i-convert-numpy-nan-objects-to-sql-nulls"""
    if not np.isnan(f):
        return _Float(f)
    return _NULL


psycopg2.extensions.register_adapter(float, nan_to_null)


# Credentials for SQL connections
@dataclass
class Credentials:
    """Class for SQL credentials"""
    username: str
    password: str
    host: str
    port: str = None
    drivername: str = "postgresql+pg8000"
    schema: str = "public"

    @property
    def gcp_postgres_host(self) -> dict:
        """creates google cloud platform connection string for postgres"""
        return dict({"unix_sock": f"{self.host}/.s.PGSQL.5432"})

    @property
    def gcp_postgres_local(self) -> dict:
        """creates localhost connection string for postgres"""
        return dict({"host": self.host, "port": self.port})


# Database class
class Pool(object):
    def __init__(self, credentials: Credentials, conn_string: str):
        if credentials.host is None:
            raise ValueError("connection to host can not be performed because there is no host name")

        self.credentials = credentials
        self.engine = sqlalchemy.create_engine(
            sqlalchemy.engine.url.URL(
                drivername=credentials.drivername,
                username=credentials.username,
                password=credentials.password,
                query=conn_string,
            )
        )


class Cursor(Pool):
    def create(self):
        Base.metadata.create_all(self.engine)

    def read_table(self, table: str) -> pd.DataFrame:
        return pd.read_sql_table(table, self.engine)

    def table_exists(self, table: str) -> bool:
        if table in self.engine.table_names():
            return True
        return False

    def upsert_table(self, table: str, model, rows):
        table = model.__table__
        statement = insert(table).values(rows)
        primary_keys = [key.name for key in inspect(table).primary_key]
        update_dict = {c.name: c for c in statement.excluded if not c.primary_key}

        statement = statement.on_conflict_do_update(
            index_elements=primary_keys,
            set_=update_dict
        )

        self.engine.execute(statement)

    def query(self, sql: str, params: Union[Tuple, dict] = None):
        return pd.read_sql((sql), self.engine, params=params)

    def max_value(self, table: str, column: str) -> Any:
        params = {"column": column}
        return self.query(f"SELECT MAX({column}) FROM {table}")

    def find_row(self, table: str, column: str, value: str) -> Any:
        params = {"value": value}
        return self.query(f"SELECT * FROM {table} WHERE {column} = %(value)s", params)
