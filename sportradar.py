import http.client
import pandas as pd
import logging

from nba import jsonstring_to_pandas

# Constants
API_URL = "api.sportradar.us"


class APICall(object):
    url = None
    method = "get"

    def __init__(self, api_key, *args, **kwargs):
        self.conn = http.client.HTTPSConnection(API_URL)
        if api_key is None:
            raise ValueError("API key is none")
        self.api_key = api_key

    def _process_response(self, response):
        return

    def call(self, **params):
        url = f"{self.url}?api_key={self.api_key}"
        r = None
        logging.info(f"{self.method.upper()} request sent to: {url}")

        self.conn.request(self.method, url)
        r = self.conn.getresponse()
        # make sure response is good
        if r.status != 200:
            raise ValueError(f"Request was not successful: Error {r.status}")
        response = r.read()
        processed = self._process_response(response)
        if processed is not None:
            response = processed
        return response


# Seasons API calls
class Seasons(APICall):
    url = "/nba/trial/v7/en/league/seasons.json"

    def _process_response(self, response):
        dataframe = jsonstring_to_pandas(response, "seasons")
        dataframe.year = year_to_seasons(dataframe.year)
        return dataframe


def year_to_seasons(series: pd.Series) -> pd.Series:
    """Formats year (YYYY) into NBA season format (YYYY-YY). This format is used
    by stats.nba.com as the season's unique identifier. 

    Example: Converts the year '2020' to '2020-21'.

    Attributes:
        series: pandas series containing years
    
    Returns:
        Pandas series with new format
    """
    prefix = series.map("{}-".format)
    suffix = (series + 1).astype(str).str.slice(2)
    return prefix + suffix
