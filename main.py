import os

from google.cloud import secretmanager

import models
import database

import nba_api.stats.endpoints
from nba import request, dataset
from sportradar import Seasons
from authentication import secrets

# Initialise secrets manager client
client = secretmanager.SecretManagerServiceClient()

# Constants
USERNAME, PASSWORD = [secrets(client, k) for k in ("db_username", "db_password")]
SPORTRADAR_API_KEY = secrets(client, "API_KEY_Sportradar")
SCRAPEFLY_API_KEY = secrets(client, "API_KEY_Scrapfly")
CONNECTION_STRING = os.environ.get("SQL_CONN_STRING", None)


def init_credentials():
    return database.Credentials(
        username=USERNAME,
        password=PASSWORD,
        host=CONNECTION_STRING
    )


# Functions
def players(event, context):
    credentials = init_credentials()
    sql = database.Cursor(credentials, credentials.gcp_postgres_host)

    players = request(nba_api.stats.endpoints.CommonAllPlayers, proxy=SCRAPEFLY_API_KEY)
    players = dataset(players, "CommonAllPlayers")

    sql.upsert_table(
        table=models.Players.__tablename__,
        model=models.Players,
        rows=players.to_dict(orient="orients"),
    )


def teams(event, context):
    credentials = init_credentials()
    sql = database.Cursor(credentials, credentials.gcp_postgres_host)

    teams = request(nba_api.stats.endpoints.CommonTeamYears, proxy=SCRAPEFLY_API_KEY)
    teams = dataset(teams, "TeamYears")
    teams.dropna(inplace=True)  # Remove NA teams

    for team in teams["team_id"]:
        team_background = request(nba_api.stats.endpoints.TeamDetails, proxy=SCRAPEFLY_API_KEY, team_id=team)
        sql.upsert_table(
            table=models.Teams.__tablename__,
            model=models.Teams,
            rows=team_background.to_dict(orient="records"),
        )


def player_gamelogs(event, context):
    credentials = init_credentials()
    sql = database.Cursor(credentials, credentials.gcp_postgres_host)
    season = sql.find_row("seasons", "status", "inprogress")

    if season is None:
        return

    gamelogs = request(nba_api.stats.endpoints.PlayerGameLogs, proxy=SCRAPEFLY_API_KEY, season_nullable=season,
                       measure_type_player_game_logs_nullable="Base")

    sql.upsert_table(
        table=models.PlayerGameLogs.__tablename__,
        model=models.PlayerGameLogs,
        rows=gamelogs.to_dict(orient="records"),
    )


def adv_player_gamelogs(event, context):
    credentials = init_credentials()
    sql = database.Cursor(credentials, credentials.gcp_postgres_host)
    season = sql.find_row("seasons", "status", "inprogress")

    if season is None:
        return

    advgamelogs = request(nba_api.stats.endpoints.PlayerGameLogs, proxy=SCRAPEFLY_API_KEY, season_nullable=season,
                          measure_type_player_game_logs_nullable="Advanced")

    sql.upsert_table(
        table=models.AdvancedPlayerGameLogs.__tablename__,
        model=models.AdvancedPlayerGameLogs,
        rows=advgamelogs.to_dict(orient="records"),
    )


# SportRadar
def seasons(event, context):
    seasons = Seasons(SPORTRADAR_API_KEY).call()
    credentials = init_credentials()
    sql = database.Cursor(credentials, credentials.gcp_postgres_host)
    sql.upsert_table("seasons", models.Seasons, seasons.to_dict(orient="records"))
