import json
import yaml

import pandas as pd

from nba_api.stats import endpoints
from errors import DataSetNotFoundError


def request(endpoint: endpoints._base.Endpoint, **kwargs) -> endpoints._base.Endpoint:
    """

    Args:
        endpoint (object):
        **kwargs: arguments to be used in the nba_api.stats.endpoint objects

    Returns:
        Endpoint (object): Base endpoint in the nba_apis package
    """
    return endpoint(**kwargs)


def dataset(results: endpoints._base.Endpoint, dataset_name: str, table_name: str = None):
    """

    Args:
        results (object): Endpoint from the nba_api package
        dataset_name: The dataset name to be used from the
        table_name:

    Returns:

    """
    available_datasets = results.get_available_data()

    if (idx := index(dataset_name, available_datasets)) > -1:
        dataframe = results.get_data_frames()[idx]
    else:
        raise DataSetNotFoundError(dataset, available_datasets)

    table_name = table_name if None else dataset_name

    return renamed(dataframe, column=table_name)


def renamed(dataframe: pd.DataFrame, column: str):
    columns = read_yaml("tables.yaml")
    columns = columns[column]
    dataframe = dataframe.rename(columns=columns)

    return dataframe[columns.values()]


def read_yaml(_file: str):
    """Opens shared columns .yaml file to get replacement columns"""
    with open(_file, "r") as stream:
        return yaml.safe_load(stream)


def jsonstring_to_pandas(string: str, key: str = None, encoding: str = "utf-8") -> pd.DataFrame:
    """

    Args:
        string:
        key:
        encoding:

    Returns:

    """
    stream = json.loads(string)
    if key in stream.keys() and is_nested(stream):
        return pd.json_normalize(stream[key], sep="_")
    return stream.get(key, ValueError(f"'{key}' not used in used json"))


def is_nested(stream: dict) -> bool:
    """
    Returns boolean if dictionary contains a nested dictionary in the first set of keys
    Args:
        stream: dict

    Returns:
        bool
    """
    for key in stream.keys():
        if isinstance(stream[key], dict):
            return True
    return False


def index(key: str, lst: list) -> int:
    """
    Returns the index of the dataset if found in the endpoint
    Args:
        key: string to find in lst
        lst: list containing available keys

    Returns:
        index: int
    """
    for idx, k in enumerate(lst):
        if key == k:
            return idx
    return -1
