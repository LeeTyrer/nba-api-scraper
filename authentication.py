import os

# Constants
PROJECT = os.environ.get("GCP_PROJECT", "nba-api-scraper")

def secrets(client, key: str):
    name = f"projects/{PROJECT}/secrets/{key}/versions/latest"
    resp = client.access_secret_version(name=name)
    return resp.payload.data.decode("UTF-8")
